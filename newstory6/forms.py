from django import forms
from .models import *

class detail_kegiatan(forms.ModelForm):
    class Meta:
        model = ListKegiatan
        fields = ['nama_kegiatan', 'deskripsi']
    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs1 = {
        'type' : 'text',
        'placeholder' : 'Nama kegiatan',
        'style' : 'background-color: #E8EEF2'
    }
    input_attrs2 = {
        'type' : 'text',
        'placeholder' : 'Deskripsi kegiatan',
        'style' : 'background-color: #E8EEF2'
    }
    nama_kegiatan = forms.CharField(label='Nama kegiatan', required=True, max_length=27, widget=forms.TextInput(attrs=input_attrs1))
    deskripsi = forms.CharField(label='Deskripsi kegiatan', required=True, max_length=27, widget=forms.Textarea(attrs=input_attrs2))

class nama_orang(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = ['nama']
    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs = {
        'type' : 'text',
        'placeholder' : 'Nama Pendaftar',
        'style' : 'background-color: #E8EEF2'
    }
    nama = forms.CharField(label='Nama pendaftar', required=True, max_length=27, widget=forms.TextInput(attrs=input_attrs))
