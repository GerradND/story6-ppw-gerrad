from django.db import models

# Create your models here.
class Peserta(models.Model):
    nama = models.CharField(max_length=200)

class ListKegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=200)
    deskripsi = models.CharField(max_length=200)
    list_orang = models.ManyToManyField(Peserta,blank=True, related_name='orang')
