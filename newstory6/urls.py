from django.urls import path
from .views import *

app_name = "newstory6"
urlpatterns = [
    path('', kegiatan, name='kegiatan'),
    path('savekegiatan', savekegiatan, name='savekegiatan'),
    path("saveorang", saveorang, name='saveorang'),
    path('deletekegiatan/<int:myid>',hapuskegiatan,name='hapuskegiatan'),
]
