from django.test import TestCase, Client

# Create your tests here.
class Story6Test(TestCase):
    def test_story6_is_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)
    def test_story6_using_Kegiatan_func(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, "Kegiatan.html")
    def test_model_can_create_new_activity(self):
        new_activity = ListKegiatan.objects.create(nama_kegiatan="Nonton", deskripsi="hayuk")
        count = ListKegiatan.objects.all().count()
        self.assertEqeual(count, 1)
    def test_model_can_create_new_peserta(self):
        new_orang = Peserta.objects.create(nama="udin")
        count = Peserta.objects.all().count()
        self.assertEqual(count, 1)
    def test_data_saved_in_webpage(self):
        response = Client().post("/story6", {'nama_kegiatan': 'Tidur', 'deskripsi':'lets go'})
        tampilan_html = response.content.decode('utf8')
        self.assertIn("Tidur", tampilan_html)
        self.assertIn("lets go", tampilan_html)
