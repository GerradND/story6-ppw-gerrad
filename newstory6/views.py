from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import *
from .models import *

# Create your views here.
response = {}
def kegiatan(request):
    form1 = detail_kegiatan()
    form2 = nama_orang()
    list_kegiatan = ListKegiatan.objects.all()
    list_orang = Peserta.objects.all()
    response['kegiatan'] = list_kegiatan
    response['orang'] = list_orang
    response['form1'] = form1
    response['form2'] = form2
    return render(request,'Kegiatan.html', response)

def savekegiatan(request):
    form = detail_kegiatan(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        response['nama_kegiatan'] = request.POST['nama_kegiatan']
        response['deskripsi'] = request.POST['deskripsi']
        list_kegiatan = ListKegiatan(nama_kegiatan=response['nama_kegiatan'], deskripsi=response['deskripsi'],)
        list_kegiatan.save()
        return HttpResponseRedirect('/story6')
    else:
        return HttpResponseRedirect('/')

def saveorang(request):
    form = nama_orang(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        response['nama'] = request.POST['nama']
        orang = Peserta(nama=response['nama'])
        orang.save()
        return HttpResponseRedirect('/story6')
    else:
        return HttpResponseRedirect('/')

def hapuskegiatan(request, myid):
    ListKegiatan.objects.get(id=myid).delete()
    return HttpResponseRedirect('/story6')
