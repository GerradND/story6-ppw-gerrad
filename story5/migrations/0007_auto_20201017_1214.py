# Generated by Django 2.0.6 on 2020-10-17 05:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('story5', '0006_auto_20201015_1132'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='jadwal',
            name='deskripsi',
        ),
        migrations.RemoveField(
            model_name='jadwal',
            name='dosen',
        ),
        migrations.RemoveField(
            model_name='jadwal',
            name='jumlah_sks',
        ),
        migrations.RemoveField(
            model_name='jadwal',
            name='ruang_kelas',
        ),
        migrations.RemoveField(
            model_name='jadwal',
            name='semester',
        ),
    ]
