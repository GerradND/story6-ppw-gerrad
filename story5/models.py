from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Jadwal(models.Model):
    mata_kuliah = models.CharField(max_length=200)
    dosen = models.CharField(max_length=200,default='none')
    jumlah_sks = models.CharField(max_length=200,default='none')
    deskripsi = models.CharField(max_length=200,default='none')
    semester = models.CharField(max_length=200,default='none')
    ruangan = models.CharField(max_length=200,default='none')

class Post(models.Model):
    author = models.ForeignKey(Jadwal, on_delete = models.CASCADE)
    content = models.CharField(max_length=125)
    published_date = models.DateTimeField(default=timezone.now)
    

