from django.urls import path
from .views import *

app_name = "story5"
urlpatterns = [
    path('',jadwal,name='jadwal'),
    path('savejadwal', savejadwal),
    path('detail/<int:myid>',add_detail,name='detail'),
    path('delete/<int:myid>',delete_detail,name='delete_detail'),
]
